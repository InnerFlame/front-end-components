A Pen created at CodePen.io. You can find this one at http://codepen.io/MarcMalignan/pen/xByvJ.

 A 3D popup message that follows the user's mouse movements.
Made in CSS3 + jQuery.

UPDATE : simplified the JS code by using a pseudo-element for the popup's shadow.