A Pen created at CodePen.io. You can find this one at http://codepen.io/chriscoyier/pen/CiflJ.

 The `<label>` is the placeholder. It shows because the input is on top of it with a transparent background. On focus we can move it, with just CSS, through an adacent sibling combinator and `:focus`. If the input is `:valid`, it can have a solid background, thus hiding the "placeholder".

Saw the design somewhere - can't remember where, but I snagged a screenshot:

![img](http://f.cl.ly/items/0S0V0j2T0n2q0l3S1e1g/Screen%20Shot%202014-02-01%20at%2012.30.49%20PM.png)
